﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaCarritoDeCompras.Models;
using Microsoft.AspNet.Identity;
using PruebaCarritoDeCompras.Models.Utilidades;
using System.Web.Helpers;
using System.IO;
using System.Web.Script.Serialization;

namespace PruebaCarritoDeCompras.Controllers
{
    public class ProductosController : Controller
    {
        public ActionResult Pureba()
        {
            Compra compra = Compra.GetCompra(7);
            return View(compra.Productos);
        }

        [HttpPost]
        public ActionResult ObtenerProducos()
        {
            Compra compra = Compra.GetCompra(7);

            return Json(new { success = true, compra = compra }, JsonRequestBehavior.AllowGet);
        }

        // GET: Productos/Details/5
        public ActionResult Details(int id)
        {
            return View(Producto.GetProducto(id));
        }

        // POST: Productos/Details
        [HttpPost]
        [Authorize]
        public ActionResult Details(Producto producto)
        {
            try
            {
                return AgregarProductoACarrito(producto);
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        // GET: Productos/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Productos/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(CreateProductoViewModel producto)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (Producto.GuardarProducto(producto))
                    {
                        WebImage image = producto.ArchivoImagen.ResizeImage(800, 800);
                        string relativeFolderPath = "/Imagenes";
                        string absoluteFolderPath = Path.Combine(Server.MapPath(relativeFolderPath));

                        if (image != null)
                        {
                            bool exists = Directory.Exists(absoluteFolderPath);
                            if (!exists) Directory.CreateDirectory(absoluteFolderPath);

                            string imageName = producto.Id + "." + image.ImageFormat;
                            string absoluteImagePath = string.Format("{0}\\{1}", absoluteFolderPath, imageName);
                            image.Save(absoluteImagePath);

                            string relativeImagePath = string.Format("{0}/{1}", relativeFolderPath, imageName);
                            producto.RutaImagen = relativeImagePath;

                            Gate.ConsultaSQL(string.Format("UPDATE Productos SET RutaImagen = '{0}' WHERE Id = {1}", producto.RutaImagen, producto.Id));
                        }


                        return RedirectToAction("Details", new { id = producto.Id });
                    }
                }
                catch
                {
                    return View("Error");
                }
            }
            return View("Error");
        }

        // GET: Productos/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Productos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Productos/Delete/5
        [Authorize]
        public ActionResult Delete(int id)
        {
            return View(Producto.GetProducto(id));
        }

        // POST: Productos/Delete/5
        [Authorize]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                if (Producto.BorrarProducto(id))
                {
                    return RedirectToAction("Index", "Home");
                }

                return View("Error");
            }
            catch
            {
                return View();
            }
        }

        [Authorize]
        public ActionResult Carrito()
        {
            var UserId = User.Identity.GetUserId();

            Carrito carrito = Models.Carrito.GetCarrito(UserId);
            
            return View(carrito);
        }

        [Authorize]
        public ActionResult AgregarProductoACarrito(int IdProducto, int cantidad = 1)
        {
            try
            {
                Producto producto = Producto.GetProducto(IdProducto);
                producto.Cantidad = cantidad;

                return AgregarProductoACarrito(producto);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult AgregarProductoACarrito(Producto producto)
        {
            try
            {
                string IdUsuario = User.Identity.GetUserId();
                Carrito carrito = Models.Carrito.GetCarrito(IdUsuario);

                if (carrito.Productos.Exists(p => p.Id == producto.Id))
                {
                    Producto tempProduct = carrito.Productos.Find(p => p.Id == producto.Id);

                    tempProduct.Cantidad += producto.Cantidad;

                    int resta = tempProduct.CantDisponible - producto.Cantidad;
                    if (resta >= 0)
                    {
                        tempProduct.CantDisponible = resta;
                    }
                    else
                        return View("Error");

                    producto.Cantidad = tempProduct.Cantidad;
                    producto.CantDisponible = tempProduct.CantDisponible;

                    Gate.ConsultaSQL(string.Format("UPDATE [Productos] SET [CantDisponible] = {0} WHERE [Id] = {1}", producto.CantDisponible, producto.Id));
                    Gate.ConsultaSQL(string.Format("UPDATE [ProductosDeCarrito]  SET [Cantidad] = {0} WHERE [IdProducto] = {1}", producto.Cantidad, producto.Id));

                    return View("Carrito", carrito);
                }
                else
                {
                    producto.CantDisponible -= producto.Cantidad;

                    if (Producto.GuardarProductoEnCarrito(producto, carrito.Id))
                    {
                        carrito.Productos.Add(producto);
                        Gate.ConsultaSQL(string.Format("UPDATE [Productos] SET [CantDisponible] = {0} WHERE [Id] = {1}", producto.CantDisponible, producto.Id));
                        return View("Carrito", carrito);
                    }
                    else
                        return View("Error");
                }
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
        
        public JsonResult AgregarAlCarrito(int IdProducto)
        {
            Producto producto = Producto.GetProducto(IdProducto);
            AgregarProductoACarrito(producto);
            return Json(new { success = true, producto = new Producto { Id = producto.Id, CantDisponible = producto.CantDisponible } , mensaje = "Se agrego el producto al carrito" }, JsonRequestBehavior.AllowGet); ;
        }

        [Authorize]
        public ActionResult QuitarProductoDeCarrito(int IdProducto)
        {
            string IdUsuario = User.Identity.GetUserId();
            Carrito carrito = Models.Carrito.GetCarrito(IdUsuario);
            Producto producto = carrito.Productos.Find(p => p.Id == IdProducto);

            if (Producto.QuitarProductoEnCarrito(producto.Id, carrito.Id))
            {
                producto.CantDisponible += producto.Cantidad;
                Gate.ConsultaSQL(string.Format("UPDATE [Productos] SET [CantDisponible] = {0} WHERE [Id] = {1}", producto.CantDisponible, producto.Id));

                carrito.Productos.RemoveAll(p => p.Id == IdProducto);
                return RedirectToAction("Carrito", carrito);
            }
            else
                return View("Error");
        }

        [HttpPost]
        public ActionResult RealizarCompra()
        {
            if (!User.Identity.IsAuthenticated)
                return Json(new { success = false, mensaje = "El usuario no ha iniciado sesión." }, JsonRequestBehavior.AllowGet); ;

            string IdUsuario = User.Identity.GetUserId();
            Carrito carrito = Models.Carrito.GetCarrito(IdUsuario);

            if (carrito.Productos.Count < 1)
            {
                return Json(new { success = false, mensaje = "No hay productos que comprar en el carrito." }, JsonRequestBehavior.AllowGet); ;
            }

            Compra compra = new Compra
            {
                IdUsuario = IdUsuario,
                Total = carrito.Total,
                Fecha = DateTime.Now,
                Productos = carrito.Productos
            };

            if (Compra.RealizarCompra(compra))
            {
                Producto.DesecharProductosDeCarrito(carrito.Id);
                return Json(new { success = true, compra = compra, mensaje = "La compra se realizo correctamente" }, JsonRequestBehavior.AllowGet); ;
            }
            else
            {
                return Json(new { success = false, mensaje = "Error al realizar la compra." }, JsonRequestBehavior.AllowGet); ;
            }
        }
    }
}
