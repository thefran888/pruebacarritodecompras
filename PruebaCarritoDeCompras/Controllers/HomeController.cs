﻿using PruebaCarritoDeCompras.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace PruebaCarritoDeCompras.Controllers
{
    public class HomeController : Controller
    {
        
        public HomeController()
        {
            //List<Producto> productos = new Producto().GetProductosCarrito("b90dbc28-8075-4a7d-a975-cf048e7f598a");
            //List<Compra> compras = new Compra().GetCompras("b90dbc28-8075-4a7d-a975-cf048e7f598a");
            //string userid = User.Identity.GetUserId();
            //Producto prodct = new Producto().GetProducto(1);
            //Carrito carrito = new Carrito().GetCarrito("b90dbc28-8075-4a7d-a975-cf048e7f598a");
            //Compra compra = new Compra().GetCompra(1);
            //Producto prodct = new Producto {
            //    Nombre = "Piña",
            //    Descripcion = "Es verde o naranja por fuera y amarilla por dentro",
            //    Precio = 45,
            //    CantDisponible = 20
            //};
            //prodct.GuardarProducto();
        }

        //[Authorize]
        public ActionResult Index()
        {
            return View(Producto.GetProductos());
        }

        [HttpPost]
        public ActionResult Busqueda(string StrBusqueda)
        {

            return View(Producto.GetProductos(string.Format("WHERE (Nombre LIKE '%' + '{0}' + '%') OR (Descripcion LIKE '%' + '{0}' + '%')", StrBusqueda)));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}