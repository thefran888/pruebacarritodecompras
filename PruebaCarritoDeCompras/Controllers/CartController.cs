﻿using Microsoft.AspNet.Identity;
using PruebaCarritoDeCompras.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PruebaCarritoDeCompras.Controllers
{
    public class CartController : Controller
    {
        [HttpGet]
        public JsonResult PayPaltoken()
        {
            if (User.Identity.IsAuthenticated)
            {
                Carrito carrito = Carrito.GetCarrito(User.Identity.GetUserId());

                if (carrito.Productos.Count < 1)
                {
                    return Json(JsonRequestBehavior.DenyGet);
                }

                string strGUID = (Session["TicketLockedId"] != null ? Session["TicketLockedId"].ToString() : "");

                PayPalOrder objPay = new PayPalOrder
                {
                    Amount = Math.Round(carrito.Total, 2),
                    OrderId = carrito.Id.ToString()
                };

                PayPalRedirect redirect = PayPal.ExpressCheckout(objPay);

                return Json(redirect.Token, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(JsonRequestBehavior.DenyGet);

        }

        public ActionResult ProductOrder()
        {
            return View();
        }

        public ActionResult CheckoutReview(string token, string PayerID)
        {
            string retMsg = "";
            string PayerID1 = "";
            PayPal.GetCheckoutDetails(token, ref PayerID1, ref retMsg);
            ViewData["ReturnMessage"] = retMsg;
            ViewData["token"] = token;
            ViewData["PayerID"] = PayerID1;

            if (User.Identity.IsAuthenticated)
            {
                Carrito carrito = Carrito.GetCarrito(User.Identity.GetUserId());
                return View(carrito);
            }
            else
            {
                return View("Error");
            }
        }

        public ActionResult DoCheckoutPayment(string token, string PayerID)
        {
            if (User.Identity.IsAuthenticated)
            {
                string IdUser = User.Identity.GetUserId();
                Carrito carrito = Carrito.GetCarrito(IdUser);

                string retMsg = "";
                string total = Math.Round(carrito.Total, 2).ToString();
                PayPal.DoCheckoutPayment(total, token, PayerID, ref retMsg);
                ViewData["ReturnMessage"] = retMsg;

                Compra compra = new Compra
                {
                    IdUsuario = IdUser,
                    Total = carrito.Total,
                    Fecha = DateTime.Now,
                    Productos = carrito.Productos
                };

                if (Compra.RealizarCompra(compra))
                {
                    Producto.DesecharProductosDeCarrito(carrito.Id);
                    return View(carrito);
                }
                else
                {
                    return View("Error");
                }

                
            }
            else
                return View("Error");
        }
    }
}