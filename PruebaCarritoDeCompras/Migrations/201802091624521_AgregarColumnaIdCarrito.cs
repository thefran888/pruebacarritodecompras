namespace PruebaCarritoDeCompras.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarColumnaIdCarrito : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IdCarrito", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IdCarrito");
        }
    }
}
