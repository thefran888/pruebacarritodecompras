﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PruebaCarritoDeCompras.Models
{
    public class Compra
    {
        public int Id { get; set; }

        public string IdUsuario { get; set; }

        public decimal Total { get; set; }

        public DateTime Fecha { get; set; }

        public List<Producto> Productos { get; set; }


        #region Metodos
        public static Compra GetCompra(int IdCompra)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            Compra compra = new Compra();
            try
            {
                conn.Open();
                cmd.CommandText = string.Format("SELECT [Id] ,[IdUsuario] ,[Total] ,[Fecha] FROM [CarritoCompras].[dbo].[EncabezadosCompras] WHERE [Id] = '{0}'", IdCompra);
                SqlDataReader reader;
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        compra = new Compra
                        {
                            Id = (int)reader["Id"],
                            IdUsuario = reader["IdUsuario"].ToString(),
                            Total = Convert.ToDecimal(reader["Total"]),
                            Fecha = Convert.ToDateTime(reader["Fecha"]),
                            Productos = Producto.GetProductosCompra(IdCompra)
                        };
                        reader.NextResult();
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                cmd = new SqlCommand("", conn);
                conn.Close();
            }
            return compra;
        }

        public static List<Compra> GetCompras(string idUsuario, string filtroAND = "")
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);

            List<Compra> compras = new List<Compra>();

            try
            {
                conn.Open();
                cmd.CommandText = string.Format("SELECT [Id] ,[IdUsuario] ,[Total] ,[Fecha] FROM [CarritoCompras].[dbo].[EncabezadosCompras] WHERE IdUsuario = '{0}' {1}", idUsuario, filtroAND);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Compra compra = new Compra
                        {
                            Id = (int)reader["Id"],
                            IdUsuario = reader["IdUsuario"].ToString(),
                            Total = Convert.ToDecimal(reader["Total"]),
                            Fecha = Convert.ToDateTime(reader["Fecha"]),
                            Productos = Producto.GetProductosCompra((int)reader["Id"])
                        };
                        compras.Add(compra);
                    }
                    reader.NextResult();
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cmd = new SqlCommand("", conn);
                conn.Close();
            }
            return compras;
        }

        public static bool RealizarCompra(Compra compra)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);

            bool r = false;

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "EncabezadosCompras_sp_Guardar";
                cmd.Parameters.AddWithValue("IdUsuario", compra.IdUsuario);
                cmd.Parameters.AddWithValue("Total", compra.Total);
                cmd.Parameters.AddWithValue("Fecha", compra.Fecha);
                cmd.Parameters.Add("@retVal", SqlDbType.Int);
                cmd.Parameters["@retVal"].Direction = ParameterDirection.Output;

                conn.Open();

                cmd.ExecuteNonQuery();

                if ((cmd.Parameters["@retVal"].Value.ToString() != "0"))
                {
                    compra.Id = Convert.ToInt32(cmd.Parameters["@retVal"].Value);
                    r = Producto.GuardarProductosEnCompra(compra.Productos, compra.Id);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                cmd = new SqlCommand("", conn);
                conn.Close();
            }
            return r;
        }
        #endregion

    }
}