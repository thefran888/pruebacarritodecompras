﻿using PruebaCarritoDeCompras.Models.Utilidades;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PruebaCarritoDeCompras.Models
{
    public class Producto
    {
        [Key]
        public int Id { get; set; } = 0;
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "El valor debe de ser mayor o igual a 0")]
        [Required(ErrorMessage = "El campo 'Precio' no puede estar vacío y debe de estar en un formato valido.")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "RD{0:C2}")]
        public decimal Precio { get; set; }

        [Display(Name = "Disponibles")]
        public int CantDisponible { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "El valor debe de ser mayor o igual a 1")]
        public int Cantidad { get; set; } = 1;
        public string RutaImagen { get; set; }



        #region Metodos
        public static Producto GetProducto(int Id)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);

            Producto producto = new Producto();
            try
            {
                conn.Open();
                cmd.CommandText = string.Format("SELECT [Id] ,[Nombre] ,[Descripcion] ,[Precio] ,[CantDisponible], [RutaImagen] FROM[CarritoCompras].[dbo].[Productos] WHERE Id = {0}", Id);
                SqlDataReader reader;
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        producto = new Producto
                        {
                            Id = (int)reader["Id"],
                            Nombre = reader["Nombre"].ToString(),
                            Descripcion = reader["Descripcion"].ToString(),
                            Precio = Convert.ToDecimal(reader["Precio"]),
                            CantDisponible = (int)reader["CantDisponible"],
                            RutaImagen = reader["RutaImagen"].ToString()
                        };
                        reader.NextResult();
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                cmd = new SqlCommand("", conn);
                conn.Close();
            }
            return producto;
        }

        public static List<Producto> GetProductos(string filtroWhere = "")
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            List<Producto> productos = new List<Producto>();

            try
            {
                conn.Open();
                cmd.CommandText = string.Format("SELECT [Id] ,[Nombre] ,[Descripcion] ,[Precio] ,[CantDisponible], [RutaImagen] FROM[CarritoCompras].[dbo].[Productos] {0}", filtroWhere);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Producto producto = new Producto
                        {
                            Id = (int)reader["Id"],
                            Nombre = reader["Nombre"].ToString(),
                            Descripcion = reader["Descripcion"].ToString(),
                            Precio = Convert.ToDecimal(reader["Precio"]),
                            CantDisponible = (int)reader["CantDisponible"],
                            RutaImagen = reader["RutaImagen"].ToString()
                        };
                        productos.Add(producto);
                    }
                    reader.NextResult();
                    reader.Close();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                cmd = new SqlCommand("", conn);
                conn.Close();
            }
            return productos;
        }
        public static List<Producto> GetProductosCompra(int IdEncabezadosCompra)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);

            List<Producto> productos = new List<Producto>();

            try
            {
                conn.Open();
                cmd.CommandText = string.Format("SELECT [IdProducto] ,[Nombre] ,[Descripcion] ,[Precio] ,[CantDisponible] ,[RutaImagen] ,[Cantidad] FROM [CarritoCompras].[dbo].[vDetallesCompras] WHERE [IdEncabezadosCompras] = {0}", IdEncabezadosCompra);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Producto producto = new Producto
                        {
                            Id = (int)reader["IdProducto"],
                            Nombre = reader["Nombre"].ToString(),
                            Descripcion = reader["Descripcion"].ToString(),
                            Precio = Convert.ToDecimal(reader["Precio"]),
                            CantDisponible = (int)reader["CantDisponible"],
                            Cantidad = (int)reader["Cantidad"],
                            RutaImagen = reader["RutaImagen"].ToString()
                        };
                        productos.Add(producto);
                    }
                    reader.NextResult();
                    reader.Close();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                cmd = new SqlCommand("", conn);
                conn.Close();
            }
            return productos;
        }
        public static List<Producto> GetProductosCarrito(string IdUsuario)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);

            List<Producto> productos = new List<Producto>();

            try
            {
                conn.Open();
                cmd.CommandText = string.Format("SELECT [IdProducto] ,[Nombre] ,[Descripcion] ,[Precio] ,[CantDisponible] ,[Cantidad] ,[IdCarrito] ,[IdUsuario] ,[RutaImagen] FROM [CarritoCompras].[dbo].[vProductosDeCarrito] WHERE [IdUsuario] = '{0}'", IdUsuario);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Producto producto = new Producto
                        {
                            Id = (int)reader["IdProducto"],
                            Nombre = reader["Nombre"].ToString(),
                            Descripcion = reader["Descripcion"].ToString(),
                            Precio = Convert.ToDecimal(reader["Precio"]),
                            CantDisponible = (int)reader["CantDisponible"],
                            Cantidad = (int)reader["Cantidad"],
                            RutaImagen = reader["RutaImagen"].ToString()
                        };
                        productos.Add(producto);
                    }
                    reader.NextResult();
                    reader.Close();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                cmd = new SqlCommand("", conn);
                conn.Close();
            }
            return productos;
        }

        public static bool GuardarProducto(Producto producto)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);

            bool r = false;

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Productos_sp_Guardar";
            cmd.Parameters.AddWithValue("Id", producto.Id);
            cmd.Parameters.AddWithValue("Nombre", producto.Nombre);
            cmd.Parameters.AddWithValue("Descripcion", producto.Descripcion);
            cmd.Parameters.AddWithValue("Precio", producto.Precio);
            cmd.Parameters.AddWithValue("CantDisponible", producto.CantDisponible);
            cmd.Parameters.Add("@retVal", SqlDbType.Int);
            cmd.Parameters.Add("@nuevoId", SqlDbType.Int);
            cmd.Parameters["@retVal"].Direction = ParameterDirection.Output;
            cmd.Parameters["@nuevoId"].Direction = ParameterDirection.Output;

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                if (cmd.Parameters["@retVal"].Value.ToString() == "1")
                {
                    producto.Id = Convert.ToInt32(cmd.Parameters["@nuevoId"].Value);
                    r = true;
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                cmd = new SqlCommand("", conn);
                conn.Close();
            }
            return r;
        }
        public static bool GuardarProductoEnCarrito(Producto producto, int IdCarrito)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);

            if (producto.Id == 0)
                return false;

            bool r = false;

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "ProductosDeCarrito_sp_Guardar";
            cmd.Parameters.AddWithValue("IdCarrito", IdCarrito);
            cmd.Parameters.AddWithValue("IdProducto", producto.Id);
            cmd.Parameters.AddWithValue("Cantidad", producto.Cantidad);
            cmd.Parameters.Add("@retVal", SqlDbType.Int);
            cmd.Parameters["@retVal"].Direction = ParameterDirection.Output;

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                if (cmd.Parameters["@retVal"].Value.ToString() == "1")
                {
                    r = true;
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                cmd = new SqlCommand("", conn);
                conn.Close();
            }
            return r;
        }
        public static bool GuardarProductosEnCompra(List<Producto> productos, int IdCompra)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);

            bool r = false;

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "DetallesCompras_sp_Guardar";
                cmd.Parameters.AddWithValue("IdEncabezadosCompras", IdCompra);
                cmd.Parameters.Add("IdProducto", SqlDbType.Int);
                cmd.Parameters.Add("Cantidad", SqlDbType.Int);
                cmd.Parameters.Add("@retVal", SqlDbType.Int);
                cmd.Parameters["@retVal"].Direction = ParameterDirection.Output;

                conn.Open();

                foreach (Producto producto in productos)
                {
                    cmd.Parameters["IdProducto"].Value = producto.Id;
                    cmd.Parameters["Cantidad"].Value = producto.Cantidad;
                    cmd.ExecuteNonQuery();

                    r = (cmd.Parameters["@retVal"].Value.ToString() == "1");
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                cmd = new SqlCommand("", conn);
                conn.Close();
            }
            return r;
        }

        public static bool BorrarProducto(int Id)
        {
            if (Id == 0)
                return false;

            try
            {
                Gate.ConsultaSQL(string.Format("DELETE FROM [Productos] WHERE [Id] = {0}", Id));
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool QuitarProductoEnCarrito(int IdProducto, int IdCarrito)
        {
            if (IdProducto == 0)
                return false;

            try
            {
                Gate.ConsultaSQL(string.Format("DELETE FROM [ProductosDeCarrito] WHERE [IdCarrito] = {0} AND [IdProducto] = {1}", IdCarrito, IdProducto));
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool DesecharProductosDeCarrito(int IdCarrito)
        {
            try
            {
                Gate.ConsultaSQL(string.Format("DELETE FROM ProductosDeCarrito WHERE [IdCarrito] = {0}", IdCarrito));
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
        


    }

    public class CreateProductoViewModel : Producto
    {
        public HttpPostedFileWrapper ArchivoImagen { get; set; }
    }

    public class RealizarCompraViewModel
    {
        public List<Producto> Productos { get; set; }
        public int IdCarrito { get; set; }
    }
}