﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace PruebaCarritoDeCompras.Models.Utilidades
{
    public static class Utilidades
    {
        public static WebImage ResizeImage(this HttpPostedFileBase file, int maxWidth = 1366, int maxHeight = 1366)
        {
            return ResizeImage(new WebImage(file.InputStream), maxWidth, maxHeight);
        }

        public static WebImage ResizeImage(this WebImage image, int maxWidth = 1366, int maxHeight = 1366)
        {
            if (image.Height <= maxHeight && image.Width <= maxWidth)
                return image;

            float aspectRatio = Convert.ToSingle(image.Width) / Convert.ToSingle(image.Height);
            int newWidth, newHeight;

            if (image.Width > maxWidth)
            {
                newWidth = maxWidth;
                newHeight = Convert.ToInt32(newWidth / aspectRatio);

                image = image.Resize(newWidth, newHeight);
            }

            if (image.Height > maxHeight)
            {
                newHeight = maxHeight;
                newWidth = Convert.ToInt32(newHeight * aspectRatio);

                image = image.Resize(newWidth, newHeight);
            }

            return image;
        }
    }
}