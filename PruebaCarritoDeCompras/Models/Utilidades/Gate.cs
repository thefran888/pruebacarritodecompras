﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PruebaCarritoDeCompras.Models.Utilidades
{
    public class Gate
    {
        static SqlConnection conn;
        static SqlCommand cmd;
        static SqlDataAdapter adapter;


        public Gate()
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            cmd = new SqlCommand("", conn);
            adapter = new SqlDataAdapter(cmd);
        }

        public static DataTable ConsultaSQL(string consulta)
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            cmd = new SqlCommand("", conn);
            adapter = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();

            try
            {
                conn.Open();
                cmd.CommandText = consulta;
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            return dt;
        }
    }
}