﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PruebaCarritoDeCompras.Models
{
    public class Carrito
    {
        public int Id { get; set; }

        public string IdUsuario { get; set; }

        public decimal Total { get; set; }

        public List<Producto> Productos { get; set; }


        #region Metodos
        public static Carrito GetCarrito(string IdUsuario)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            Carrito carrito = new Carrito();
            try
            {
                conn.Open();
                cmd.CommandText = string.Format("SELECT [Id] ,[IdUsuario] FROM [CarritoCompras].[dbo].[Carritos] WHERE [IdUsuario] = '{0}'", IdUsuario);
                SqlDataReader reader;
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        carrito = new Carrito
                        {
                            Id = (int)reader["Id"],
                            IdUsuario = reader["IdUsuario"].ToString(),
                            Productos = Producto.GetProductosCarrito(reader["IdUsuario"].ToString())
                        };
                        reader.NextResult();
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                cmd = new SqlCommand("", conn);
                conn.Close();
            }

            carrito.CalcularTotalCarrito();
            return carrito;
        }

        #endregion
    }

    public static class ExtencionesCarrito
    {
        public static decimal CalcularTotalCarrito(this Carrito carrito)
        {
            foreach (var producto in carrito.Productos)
            {
                carrito.Total += (producto.Cantidad * producto.Precio);
            }

            return carrito.Total;
        }
    }

}