﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PruebaCarritoDeCompras.Startup))]
namespace PruebaCarritoDeCompras
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
